#!/bin/bash -x
PATH="/sbin:/bin:/usr/sbin:/usr/local/sbin:/usr/bin:/usr/local/bin"
#------------------------------------------------------------------#

# SCHROOT
OPERATION=$1
ENVNAME=$2
USER=$3

list() {
	echo "List of available schroot ENVIROMENTS..."
	schroot -l	
}
usage () {
	echo "Usage: $0 {list|new|close|attach|update|upgrade|config} ENVIROMENT USER"
	list
	exit 1	
}
new () {
    schroot -b -c source:${ENVNAME} -n ${ENVNAME} -u root --directory /root
}
close () {
    schroot -e -c ${ENVNAME}
}
attach () {
  case "$USER" in
	root)
		DIRECTORY=/root
		;;
	*)
		DIRECTORY=/home/${USER}
		;;
  esac
    schroot -r -c ${ENVNAME} -u ${USER} --directory $DIRECTORY
}
update () {
    schroot -r -c ${ENVNAME} -u ${USER} --directory /root -- /bin/bash -c 'aptitude update && aptitude safe-upgrade'
}
upgrade () {
    schroot -r -c ${ENVNAME} -u ${USER} --directory /root -- /bin/bash -c 'aptitude update && aptitude full-upgrade'
}
config () {
    echo 'Configuring locales...'
    schroot -c source:${ENVNAME} -u ${USER} --directory /root -- /bin/bash -c "
    echo 'locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8, es_CO.UTF-8 UTF-8' | /usr/bin/debconf-set-selections
    echo 'locales locales/default_environment_locale select en_US.UTF-8' | /usr/bin/debconf-set-selections
    [ -e /etc/locale.gen ] && {
        sed -i -e '/# en_US.UTF-8 UTF-8/ s//en_US.UTF-8 UTF-8/' /etc/locale.gen
        sed -i -e '/# es_CO.UTF-8 UTF-8/ s//es_CO.UTF-8 UTF-8/' /etc/locale.gen
        dpkg-reconfigure -f noninteractive locales
    } || {
        dpkg-reconfigure locales
    }
    echo 'Setting Zone infor to America/Bogota...'
    cat /usr/share/zoneinfo/America/Bogota > /etc/localtime
    date
    echo 'Installing needed packages...'
    aptitude update && aptitude -y full-upgrade && aptitude -y install vim-nox less wget curl ca-certificates
    "
}

[ $# -lt 3 ] && {
	usage
} || {
	case "$OPERATION" in
  new)
	new
	;;
  close)
	close
	;;
  attach)
	attach
	;;
  update)
	update
	;;
  upgrade)
	upgrade
	;;
  config)
	config
	;;
  *)	
	list
	;;
esac
}
exit 0

